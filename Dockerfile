FROM python:3.12-slim
MAINTAINER Rattew 'kaitrg@yahoo.com'
RUN apt-get update -qy
RUN apt-get install -qy python3 python3-pip python3-dev
COPY . /app
WORKDIR /app
COPY requirements.txt .
RUN pip install -r requirements.txt
CMD gunicorn main:app --reload --workers 4 --worker-class uvicorn.workers.UvicornWorker --bind=0.0.0.0:8000
